from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'', include(('main.urls', 'main'), namespace='main')),
    url(r'^admin/', admin.site.urls),
]
