# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json

from django import urls
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
import requests


def index(request):
    return redirect('main:login-user')


def login_user(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return login_redirect(request)
        else:
            return render(request, 'auth/login.html', {'error_message': 'Invalid credentials',
                                                       'next_url': request.GET.get('next')})
    if request.method == 'GET':
        return login_redirect(request)


def logout_user(request):
    logout(request)
    return redirect(urls.reverse('main:login-user'))


def login_redirect(request):
    if request.user and request.user.is_authenticated:
        if request.user.is_active:
            if request.GET.get('next'):
                return redirect(request.GET.get('next'))
            else:
                return redirect('main:home')
        else:
            return render(request, 'auth/login.html', {'error_message': 'Your account has been disabled',
                                                       'next_url': request.GET.get('next')})
    else:
        return render(request, 'auth/login.html', {'next_url': request.GET.get('next')})


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('main:home')
    else:
        form = UserCreationForm()
    return render(request, 'auth/register.html', {'form': form})


def home(request):
    user = request.user
    if user and user.is_authenticated and user.is_active:
        search_query = request.GET.get('q')
        if search_query:
            payload = {'api_key': 'd40368ae3c740d999a22fde1a405a24e', 'query': search_query}
            data = requests.get('https://api.themoviedb.org/3/search/movie', params=payload).text
            results = json.loads(data)['results']
            return render(request, 'home/home.html', {'results': results, 'q': search_query})
        else:
            return render(request, 'home/home.html', {'q': ''})
    else:
        return login_redirect(request)