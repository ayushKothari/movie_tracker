from django.conf.urls import url

from main import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^login/$', views.login_user, name='login-user'),
    url(r'^logout/$', views.logout_user, name='logout-user'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^home/$', views.home, name='home'),
]
