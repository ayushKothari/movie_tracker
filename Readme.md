## Movie Lookup

A django app to search for movies based on keywords.

### Setup Instuctions

- Clone the repo
- Create a virtualenv `virtualenv .`
- Start the virtual env `source bin/activate`
- Install dependencies `pip install -r requirements.txt`
- Make migrations `python manage.py migrate`
- Run server `python manage.py runserver` and go to `http://localhost:8000`.
